import path from "path";
import glob from "glob";
import jsx from "acorn-jsx";
import typescript from "@rollup/plugin-typescript";
import {getBabelOutputPlugin} from "@rollup/plugin-babel";
import styles from "rollup-plugin-styles";

const SRC = "./src";
const OUT = "./es";
const entries = glob.sync(`${SRC}/components/**/*/index.{ts,tsx}`);

/**
 * @see https://rollupjs.org/guide/en/#big-list-of-options
 */
export default {
  input: entries.reduce((obj, entry) => {
    const dest = entry.replace(new RegExp("^\\" + SRC + "/"), "").replace(/\.tsx?$/, "");
    obj[dest] = entry;
    return obj;
  }, {}),
  output: {
    dir: OUT,
    format: "es",
    entryFileNames: "[name].js",
    chunkFileNames: "utils/[name].js",
    assetFileNames: "[name][extname]"
  },
  // 解析jsx语法
  acornInjectPlugins: [jsx()],
  plugins: [
    // https://github.com/rollup/plugins/tree/master/packages/typescript
    typescript({jsx: "preserve"}),
    // https://github.com/rollup/plugins/tree/master/packages/babel
    getBabelOutputPlugin({
      configFile: path.resolve(__dirname, "babel.config.js")
    }),
    // https://github.com/Anidetrix/rollup-plugin-styles
    styles({
      minimize: true,
      mode: ["extract"],
      autoModules: /\.module\.scss$/
    })
  ],
  external: ["vue", "element-ui", "tslib"]
};