# Changelog

## 1.0.0 (2021-06-30)

* migrate vue components from @web-io/common
* 基于element-ui 2.x封装Dialog/Form/Table公共组件
* 配置rollup打包 + ts开发环境
* 修改vue单文件组为渲染函数形式
* 配置[@vue/babel-preset-jsx](https://github.com/vuejs/jsx)修改渲染函数为tsx形式
* upgrade @web-io/lint to 1.0.0