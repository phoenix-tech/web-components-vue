module.exports = {
  presets: [
    [
      // 由于vue jsx插件自动输入h参数只能在`对象简写`形式才起效
      // 所以将tsconfig target设置为esnext，使用babel preset env翻译代码为es5
      // const obj = {render () {}} => ok
      // const obj = {render: function () {}} => not work
      "@babel/preset-env",
      // Dynamic import can only be supported when transforming ES modules to AMD
      // , CommonJS or SystemJS. Only the parser plugin will be enabled.
      {modules: false}
    ],
    // https://github.com/vuejs/jsx
    [
      "@vue/babel-preset-jsx",
      {
        compositionAPI: false,
        injectH: true,
        functional: true,
        vModel: true,
        vOn: true
      }
    ]
  ],
  plugins: [
    // jsx语法解析
    // "@babel/plugin-syntax-jsx",
    "@babel/plugin-transform-runtime"
  ]
};