# @web-io/components-vue

> 基于Vue 2.x + Element UI 2.x 框架的公共组件

## 使用

访问[Home](https://gitee.com/phoenix-tech/web-components-vue/wikis/Home)查看详细使用。

## 变更日志

访问[CHANGELOG](CHANGELOG.md)查看更多。