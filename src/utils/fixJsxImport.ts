/**
 * TODO: fix acorn-jsx组件import去除问题
 * import {Button, Dialog} from "element-ui";
 * to import "element-ui"
 *
 * @param rest
 */
function fixJsxImport (...rest: unknown[]) {
  for (let i = 0; i < rest.length; i++) {
    if (!rest[i]) {
      return true;
    }
  }
  return false;
}

export default fixJsxImport;