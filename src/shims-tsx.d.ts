import Vue, {VNode} from "vue";

declare global {
  namespace JSX {
    interface Element extends VNode {}
    interface ElementClass extends Vue {}
    interface IntrinsicElements {
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      [elem: string]: any;
    }
  }
}

declare module "vue/types/options" {
  // fix TS2769: No overload matches this call.
  // Property 'xxx' does not exist on type 'ComponentOptions<Vue, DefaultData<Vue>,
  // DefaultMethods<Vue>, DefaultComputed, PropsDefinition<Record<string, any>>, Record<...>>'.
  interface ComponentOptions {
    // https://github.com/vuejs/jsx/tree/dev/packages/babel-plugin-transform-vue-jsx
    ref?: string;
    key?: string | number;
    slot?: string;
    class?: string | (string | Record<string, boolean>)[] | Record<string, boolean>;
    style?: Record<string, string | number>;
    id?: string;

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    [propName: string]: any;
  }
}