import Vue, {VNode, PropType, CreateElement} from "vue";
import {ScopedSlot, VNodeData, VNodeChildren} from "vue/types/vnode";
import {Table, TableColumn, Pagination, Loading} from "element-ui";
import fixJsxImport from "../../utils/fixJsxImport";
import styles from "./index.module.scss";

fixJsxImport(Table, TableColumn, Pagination);

Loading.install(Vue);

interface This extends Vue {
  _selectedData: unknown[];
  _currentRow: null | unknown;

  tableData: unknown[];
  total: number;
  data: unknown[] | null;
  autoLoad: boolean;
  mCurrentPage: number;
  mPageSize: number;
  loading: boolean;
  pageList: (
    opts: {page: number; pageSize: number;},
    ...rest: unknown[]
  ) => Promise<{
    tableData: unknown[];
    total: number;
    page?: number;
    pageSize?: number;
  }>;
}

interface ElTable {
  setCurrentRow: (row: unknown) => void;
  clearSelection: () => void;
}

export interface Column<Row = unknown> {
  label: string;
  prop?: string;
  align?: "left" | "center" | "right";
  formatter?: (row: Row) => unknown;
  /**
   * px单位宽度, 注意不带px
   */
  width?: string | number;
  /**
   * px单位宽度, 注意不带px
   */
  minWidth?: string | number;
  /**
   * 自定义列表头渲染函数
   */
  renderHeader?: (row: Row) => unknown;
  /**
   * 自定义列插槽name
   */
  slot?: string;
  /**
   * 自定义列表头插槽name
   */
  header?: string;
  hidden?: boolean;
}

export default Vue.extend({
  name: "Table",
  props: {
    currentPage: {
      type: Number,
      default: 1
    },
    pageSize: {
      type: Number,
      default: 10
    },
    pageSizes: {
      type: Array as PropType<number[]>,
      default () {
        return [10, 20, 30, 40, 50];
      }
    },
    /**
     * 如果外部传递data优先用响应式table data
     * pageList就可以不用传递
     */
    data: {
      type: Array as PropType<unknown[]>,
      default: null
    },
    /**
     * 表格列定义函数
     * column options
     * {string} label
     * {string} align
     * {string} prop
     * {function} formatter
     * {string} width px单位宽度, 注意不带px
     * {string} minWidth px单位宽度, 注意不带px
     * {boolean} hidden
     * {string} slot 自定义列插槽name
     * {string} header 自定义列表头插槽name
     * {function} renderHeader 自定义列表头渲染函数
     */
    columns: {
      type: Function as PropType<() => Column[]>,
      default: null
    },
    /**
     * 表格数据获取函数
     */
    pageList: {
      type: Function as PropType<(...rest: unknown[]) => Promise<void>>,
      default: null
    },
    rowKey: {
      type: Function as PropType<(row: unknown) => string | number>,
      default: null
    },
    border: {
      type: Boolean,
      default: true
    },
    /**
     * 是否提供行选择复选框
     */
    withSelection: {
      type: Boolean,
      default: false
    },
    withIndex: {
      type: Boolean,
      default: false
    },
    /**
     * index表头名称，默认为序号
     */
    indexName: {
      type: String,
      default: "序号"
    },
    /**
     * 默认下单页时不显示分页组件
     */
    withPagination: {
      type: Boolean,
      default: true
    },
    /**
     * 是否自动调用pageList加载分页数据
     * 默认true
     */
    autoLoad: {
      type: Boolean,
      default: true
    },
    highlightCurrentRow: {
      type: Boolean,
      default: false
    },
    /**
     * @see https://element.eleme.cn/#/zh-CN/component/table#table-attributes
     */
    cellStyle: {
      type: Function as PropType<(
        {row, column, rowIndex, columnIndex}: {row: unknown; column: unknown; rowIndex: number; columnIndex: number;}
      ) => Record<string, string | number>>,
      default: undefined
    },
    pagerCount: {
      type: Number,
      default: 7
    },
    showOverflowTooltip: {
      type: Boolean,
      default: true
    },
    /**
     * element ui el-table-column插槽在渲染函数中$parent失效
     * 请通过actions属性传递操作列VNodeData和VNodeChildren
     */
    actions: {
      type: [Object, Array] as PropType<VNodeData | [VNodeData, VNodeChildren]>,
      default: undefined
    },
    "row-click": {
      type: Function as PropType<(row: unknown, column: unknown, event: Event) => void>,
      default: null
    },
    "current-change": {
      type: Function as PropType<(currentRow: unknown, oldRow: unknown) => void>,
      default: null
    },
    "check-list": {
      type: Function as PropType<(selectedData: unknown[]) => void>,
      default: null
    },
    "header-click": {
      type: Function as PropType<(column: unknown, event: Event) => void>,
      default: null
    }
  },
  data () {
    return {
      mCurrentPage: this.currentPage,
      mPageSize: this.pageSize,
      total: 0,
      mPageSizes: this.pageSizes,
      tableData: [],
      loading: false
    };
  },
  beforeCreate (this: This) {
    this._selectedData = [];
    this._currentRow = null;
  },
  mounted (this: This) {
    // 如果外部传递了table data数据源
    // 优先以外部响应式数据更新table data
    if (!this.data && this.autoLoad) {
      pageList.call(this);
    }
  },
  computed: {
    mData (): unknown[] {
      return this.data || this.tableData;
    },
    mColumns () {
      // 修复v-if和v-for共用lint错误
      // The 'mColumns' variable inside 'v-for' directive should be replaced with a computed property that returns filtered array
      // instead. You should not mix 'v-for' with 'v-if'  vue/no-use-v-if-with-v-for
      return (this.columns() as Column[])
        // hidden属性控制列显示隐藏
        ?.filter(column => !column.hidden);
    }
  },
  methods: {
    handlePageChg (this: This, currentPage: number) {
      return pageChg.call(this, currentPage, this.mPageSize);
    },

    handleSizeChg (this: This, pageSize: number) {
      return pageChg.call(this, this.mCurrentPage, pageSize);
    },

    handleSelectionChg (this: This, selectedData: unknown[]) {
      this._selectedData = selectedData;
      this.$emit("check-list", selectedData);
    },

    handleCurrentChange (currentRow: unknown, oldRow: unknown) {
      this.$emit("current-change", currentRow, oldRow);
    },

    handleRowClk (this: This, row: unknown, column: unknown, event: Event) {
      // row click事件晚于current-change事件
      if (this._currentRow === row) {
        // toggle select
        (this.$refs.table as unknown as ElTable).setCurrentRow(null);
        this._currentRow = null;
      } else {
        this._currentRow = row;
      }

      this.$emit("row-click", row, column, event);
    },

    handleHeaderClick (column: unknown, event: Event) {
      this.$emit("header-click", column, event);
    },

    // 对外API
    // ===================================

    /**
     * 外部组件通过ref主动更新table data
     */
    pageLists (this: This, ...rest: unknown[]) {
      return pageList.apply(this, rest);
    },

    /**
     * 获取选中行数据
     * @returns {array<*>}
     */
    selectedData (this: This) {
      return this._selectedData;
    },

    clearSelection (this: This) {
      (this.$refs.table as unknown as ElTable).clearSelection();
      this._selectedData = [];
      return this;
    },

    getData () {
      return this.mData;
    }
  },
  render (h: CreateElement) {
    let ActionNode: VNode[] | undefined;
    if (this.actions) {
      let args: [VNodeData, VNodeChildren?];
      if (Array.isArray(this.actions)) {
        args = this.actions;
      } else {
        args = [this.actions];
      }
      ActionNode = [h(TableColumn, ...args)];
    } else {
      ActionNode = this.$slots.actions;
    }

    return (
      <div class={styles.container}>
        <Table
          ref="table" propsData={this.mData} propsRowKey={this.rowKey}
          propsBorder={this.border} propsHighlightCurrentRow={this.highlightCurrentRow}
          propsCellStyle={this.cellStyle} vLoading={this.loading}
          onCurrent-change={this.handleCurrentChange} onSelection-change={this.handleSelectionChg}
          onRow-click={this.handleRowClk} onHeader-click={this.handleHeaderClick}
        >
          {this.withSelection && <TableColumn propsType="selection" propsWidth={50}/>}
          {this.withIndex && <TableColumn propsType="index" propsLabel={this.indexName}
            scopedSlots={{
              default: (scope: {$index: number;}) => {
                return indexMethod(scope.$index, this.mCurrentPage, this.mPageSize).toString();
              }
            }}/>
          }
          {this.mColumns.map(column => {
            const scopedSlots: Record<string, ScopedSlot> = {};
            if (column.header) {
              scopedSlots.header = () => {
                return this.$scopedSlots[column.header as string]?.({column});
              };
            }
            if (column.slot) {
              scopedSlots.default = ({row}) => {
                return this.$scopedSlots[column.slot as string]?.({row});
              };
            }

            return (
              <TableColumn
                key={column.label} propsShowOverflowTooltip={this.showOverflowTooltip}
                propsAlign={column.align} propsProp={column.prop} propsFormatter={column.formatter}
                propsLabel={column.label} propsWidth={column.width} propsMinWidth={column.minWidth}
                propsRenderHeader={column.renderHeader}
                scopedSlots={scopedSlots}
              />
            );
          })}
          {ActionNode}
        </Table>
        {this.withPagination && (
          <div class={styles.paginationBox}>
            <Pagination
              propsCurrentPage={this.mCurrentPage} propsPageSizes={this.mPageSizes}
              propsPageSize={this.mPageSize} propsTotal={this.total}
              propsPagerCount={this.pagerCount} propsLayout={"total, sizes, prev, pager, next, jumper"}
              onCurrent-change={this.handlePageChg} onSize-change={this.handleSizeChg}
            />
          </div>
        )}
      </div>
    );
  }
});

function pageChg (this: This, currentPage: number, pageSize: number) {
  this.mCurrentPage = currentPage;
  this.mPageSize = pageSize;
  pageList.call(this);
  return this;
}

async function pageList (this: This, ...rest: unknown[]) {
  let result = undefined;

  try {
    this.loading = true;
    result = await this.pageList({
      // 后端统一为page名字
      page: this.mCurrentPage,
      pageSize: this.mPageSize
      // 传递额外参数到外部pageList方法
    }, ...rest) || {tableData: [], total: 0};
  } finally {
    this.loading = false;
  }

  const {
    tableData,
    page,
    pageSize,
    total
  } = result;

  if (tableData !== undefined) {
    this.tableData = tableData;
  }
  if (page !== undefined) {
    this.mCurrentPage = page;
  }
  if (pageSize !== undefined) {
    this.mPageSize = pageSize;
  }
  if (total !== undefined) {
    this.total = total;
  }
}

// 表格序号
function indexMethod (index: number, page: number, pageSize: number) {
  return index + 1 + (page - 1) * pageSize;
}