import Vue, {PropType} from "vue";
import {Button, Dialog} from "element-ui";
import fixJsxImport from "../../utils/fixJsxImport";
import styles from "./index.module.scss";

fixJsxImport(Dialog, Button);

export default Vue.extend({
  name: "Dialog",
  props: {
    width: [String, Number] as PropType<string | number>,
    title: String,
    visible: {
      type: Boolean,
      default: true
    },
    showClose: {
      type: Boolean,
      default: true
    },
    /**
     * class name[s]
     */
    className: {
      type: [String, Array] as PropType<string | string[]>,
      default: undefined
    },
    closeOnClickModal: {
      type: Boolean,
      default: false
    },
    /**
     * 是否显示对话框底部，默认显示
     */
    withFooter: {
      type: Boolean,
      default: true
    },
    confirmTxt: {
      type: String,
      default: "确认"
    },
    cancelTxt: {
      type: String,
      default: "取消"
    },
    "on-confirm": {
      type: Function as PropType<() => void>,
      default: null
    },
    "on-close": {
      type: Function as PropType<() => void>,
      default: null
    }
  },
  computed: {
    classNames (): string {
      const classes = [styles.container];
      const className = this.className;

      if (typeof className === "string") {
        classes.push(className);
      } else if (Array.isArray(className)) {
        classes.push(...className);
      }

      return classes.join(" ");
    }
  },
  methods: {
    handleConfirm () {
      this.$emit("on-confirm");
    },
    handleCancel () {
      this.$emit("on-close");
    }
  },
  render () {
    return (
      <Dialog
        class={this.classNames} propsVisible={this.visible} propsWidth={this.width}
        propsTitle={this.title} propsShowClose={this.showClose} propsCloseOnClickModal={this.closeOnClickModal}
        propsAppendToBody={true}
        onClose={this.handleCancel}
      >
        {this.$slots.default}
        {this.withFooter ? (
          <div class={styles.footer} slot="footer">
            {this.$slots.footer ? this.$slots.footer : (
              <div>
                <Button propsType="primary" onClick={this.handleConfirm}>{this.confirmTxt}</Button>
                <Button onClick={this.handleCancel}>{this.cancelTxt}</Button>
              </div>
            )}
          </div>
        ) : null}
      </Dialog>
    );
  }
});

const mixin = Vue.extend({
  data () {
    return {
      dialogVisible: false
    };
  },
  methods: {
    showDialog () {
      this.dialogVisible = true;
    },
    closeDialog () {
      this.dialogVisible = false;
    },
    toggleDialog () {
      this.dialogVisible = !this.dialogVisible;
    }
  }
});

export {mixin};