import Vue, {PropType} from "vue";
import {
  Form, Row, Col, FormItem, DatePicker, Select,
  Option as ElOption1, Input
} from "element-ui";
import fixJsxImport from "../../utils/fixJsxImport";
import styles from "./index.module.scss";

// babel会自动优化代码，将ElOption改成Option
// 重新赋值保留ElOption
const ElOption = ElOption1;
fixJsxImport(Form, Row, Col, FormItem, DatePicker, Select, ElOption, Input);

export type Option = |
// {value: "N", label: "未过卡"}
{
  value: unknown;
  label: string | number;
} |
// 两个元素数组[value, label]，如["N", "未过卡"]
[string | number, unknown] |
// 只有一个key的对象{value: label}，{"N": "未过卡"}
// 只能支持字符串类型的value？
Record<string| number, unknown>;

/**
 * @see https://element.eleme.cn/#/zh-CN/component/form#biao-dan-yan-zheng
 * @see https://github.com/yiminghe/async-validator
 */
export interface Rule {
  required?: boolean;
  message?: string;
  trigger?: "change" | "blur";
  validator?: (rule: unknown, value: unknown, callback: (err?: Error) => void) => void | boolean | Error | Error[];
}

/**
 * 表单条目函数
 * 条目对象属性如下：
 * {string} prop
 * {string} label
 * {enum} type: input, daterange, select
 * 默认input
 * {boolean} required
 * {boolean} disabled
 * {object|array<object>} rules
 * {number} span, el-col span, 默认值6/8
 * {*|function} default, 字段默认值可以为函数
 * {array<{value: *, label: *}>} options, select组件options
 * option类型为object或者只包含两个元素的array
 * 第一元素为value，第二个元素为label
 * {string} slot 插槽name
 * {boolean} filterable
 * {boolean} remote
 * {boolean} loading
 * {function} remoteMethod,
 * {string|array<string>} classes form item classes
 * {string} valueFormat 日期值格式化字符串，默认`yyyy-MM-dd`
 * {string} format 日期显示格式化字符串
 * {function} blur
 * {function} input
 */
export interface Item {
  prop: string;
  label: string;
  // 默认input
  type?: "input" | "daterange" | "date" | "select";
  required?: boolean;
  disabled?: boolean;
  rules?: Rule | Rule[];
  // el-col span, 默认值6/8
  span?: number;
  // 字段默认值可以为函数
  default?: unknown | (() => unknown);
  // select组件options
  options?: Option[];
  // 插槽name
  slot?: string;
  filterable?: boolean;
  remote?: boolean;
  loading?: boolean;
  remoteMethod?: () => void;
  // form item classes
  classes?: string | string[];
  // 日期值格式化字符串，默认`yyyy-MM-dd`
  valueFormat?: string;
  // 日期显示格式化字符串
  format?: string;
  blur?: () => void;
  input?: () => void;
}

interface ElForm {
  resetFields: () => void;
  clearValidate: () => void;
  validate: () => void;
}

function defaultModel (items: Item[]) {
  const model: Record<string, unknown> = {};
  if (!Array.isArray(items)) {
    return model;
  }

  items.forEach(item => {
    let dftVal: unknown = "";
    if (item.hasOwnProperty("default")) {
      dftVal = item.default;
      if (typeof dftVal === "function") {
        dftVal = dftVal();
      }
    }
    model[item.prop] = dftVal;
  });

  return model;
}

function optionValue (option: Option) {
  if (Array.isArray(option) && option.length === 2) {
    // 两个元素数组，如["N", "未过卡"]
    return option[0];
  } else if (Object.keys(option).length === 1) {
    // 只有一个key的对象{"N": "未过卡"}
    // 只能支持字符串类型的value？
    return Object.keys(option)[0];
  } else if ("value" in option) {
    // {value: "N", label: "未过卡"}
    return option.value;
  }
  return "";
}

function optionLabel (option: Option) {
  if (Array.isArray(option) && option.length === 2) {
    return option[1] as string | number;
  } else if (Object.keys(option).length === 1) {
    const key = Object.keys(option)[0] as keyof Option;
    return option[key] as string | number;
  } else if ("label" in option) {
    return option.label as string | number;
  }
  return "";
}

export default Vue.extend({
  name: "Form",
  props: {
    labelPosition: {
      type: String as PropType<"left" | "center" | "right">,
      default: "right"
    },
    labelWidth: {
      type: [String, Number],
      default: "120px"
    },
    model: Object as PropType<Record<string, unknown>>,

    items: Function as PropType<() => Item[]>,

    /**
     * 如果使用渲染函数需要手动监听`change`事件处理form model变化
     */
    change: {
      type: Function as PropType<(model: Record<string, unknown>) => void>,
      default: undefined
    }
  },
  methods: {
    resetFields () {
      return (this.$refs.form as unknown as ElForm).resetFields();
    },
    clearValidate () {
      return (this.$refs.form as unknown as ElForm).clearValidate();
    },
    validate () {
      return (this.$refs.form as unknown as ElForm).validate();
    }
  },
  render () {
    return (
      <div class={styles.container}>
        <Form
          ref="form" {...{
            // fix Component with model props throws error
            // https://github.com/vuejs/jsx/issues/49
            props: {
              model: this.model,
              labelPosition: this.labelPosition,
              labelWidth: this.labelWidth
            }
          }}
        >
          <Row>
            {this.items().map(item => {
              return (
                <Col key={item.prop} propsSpan={item.span !== undefined ? item.span : 6 / 8}>
                  {
                    item.slot ? this.$scopedSlots[item.slot]?.({item}) :
                      <FormItem
                        class={item.classes} propsRequired={item.required} propsLabel={item.label}
                        propsProp={item.prop} propsRules={item.rules}
                      >
                        {
                          /^(daterange|date)$/.test(item.type as string) ?
                            <DatePicker
                              class={[styles.dateBox, (styles[item.type as string] || "")]}
                              propsValue={this.model[item.prop]} propsDisabled={item.disabled}
                              propsType={item.type} propsValueFormat={item.valueFormat || "yyyy-MM-dd"}
                              propsFormat={item.format} propsRangeSeparator={"-"}
                              propsStartPlaceholder={"开始日期"} propsEndPlaceholder={"结束日期"}
                              onInput={(val: [string | Date, string | Date]) => {
                                this.model[item.prop] = val;
                                this.$emit("change", {...this.model});
                              }}/> : item.type === "select" ? (
                              <Select
                                propsValue={this.model[item.prop]} propsDisabled={item.disabled}
                                propsFilterable={item.filterable} propsRemote={item.remote}
                                propsRemoteMethod={item.remoteMethod} propsLoading={item.loading}
                                onChange={(val: string | number) => {
                                  this.model[item.prop] = val;
                                  this.$emit("change", {...this.model});
                                }}
                              >
                                {item.options?.map(option => {
                                  const value = optionValue(option);
                                  const label = optionLabel(option);
                                  return <ElOption key={label} propsLabel={label} propsValue={value}/>;
                                })}
                              </Select>) : (
                              <Input
                                propsValue={this.model[item.prop]} propsDisabled={item.disabled}
                                onInput={(val: string | number) => {
                                  this.model[item.prop] = val;
                                  this.$emit("change", {...this.model});
                                  item.input?.();
                                }}
                                onBlur={() => { item.blur?.(); }}
                              />)
                        }
                      </FormItem>
                  }
                </Col>
              );
            })}
          </Row>
        </Form>
      </div>
    );
  }
});

export {defaultModel, optionValue, optionLabel};