declare module "*.module.scss" {
  interface Selectors {
    readonly [selector: string]: string;
  }

  const selectors: Selectors;
  export = selectors;
}